Package.describe({
    name: 'molosc:pikaday',
    version: '0.0.6',
    // Brief, one-line summary of the package.
    summary: 'Datetime Picker for Meteor',
    // URL to the Git repository containing the source code for this package.
    git: 'https://Molosc@bitbucket.org/Molosc/pikaday.git',
    // By default, Meteor will default to using README.md for documentation.
    // To avoid submitting documentation, set this field to null.
    documentation: 'README.md'
});

Package.onUse(function(api) {
    api.versionsFrom('1.2.1');
    api.use('ecmascript');
    api.use('momentjs:moment@2.10.6');
    api.addFiles(['pikaday.js', 'pikaday.css'], 'client');
    api.export('Pikaday', 'client');
});

Package.onTest(function(api) {
    api.use('ecmascript');
    api.use('tinytest');
    api.use('molosc:pikaday');
    api.addFiles('pikaday-tests.js');
});
